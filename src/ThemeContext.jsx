import React, { createContext, useContext, useState } from 'react';

const ThemeContext = createContext();

export function useTheme() {
    return useContext(ThemeContext);
}

export const ThemeProvider = ({ children }) => {
    const [theme, setTheme] = useState('light');

    const toggleTheme = () => {
        setTheme((prevTheme) => (prevTheme === 'light' ? 'dark' : 'light'));
    };

    const bodyBackgroundColor = theme === 'dark' ? 'var(--body-background-dark)' : 'var(--body-background-light)';

    return (
        <ThemeContext.Provider value={{ theme, toggleTheme }}>
            <style>
                {`
                    body {
                        background-color: ${bodyBackgroundColor};
                    }
                `}
            </style>
            {children}
        </ThemeContext.Provider>
    );
};
