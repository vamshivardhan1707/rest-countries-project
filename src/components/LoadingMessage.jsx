import React from "react";
import { useTheme } from "../ThemeContext";

export default function LoadingMessage() {
  const { theme } = useTheme();
  const isDarkMode = theme === "dark";

  return (
    <h2 className={`message ${isDarkMode ? "dark" : "light"}`}>Loading...</h2>
  );
}
