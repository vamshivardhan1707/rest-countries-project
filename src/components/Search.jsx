import React from "react";
import './Search.css'
import { useTheme } from "../ThemeContext";

export default function Search({ searchValue, onSearchChange }) {
    const {theme} = useTheme();
    const isDarkMode = theme === 'dark';

    return (
        <div className="search">
            <input type="text" 
                placeholder="Search for a country..."
                value={searchValue}
                onChange={onSearchChange}
                className={`input ${isDarkMode ? 'dark' : 'light'}`} />
        </div>
    )
}