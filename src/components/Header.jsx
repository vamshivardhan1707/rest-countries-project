import React from "react";
import "./Header.css";
import { useTheme } from "../ThemeContext.jsx";
import iconSun from "/src/assets/sunny-outline.svg"
import iconMoon from "/src/assets/moon-outline.svg"

const Header = () => {
  const { theme, toggleTheme } = useTheme();

  const isDarkMode = theme === "dark";

  return (
    <div className={`header-content ${isDarkMode ? "dark" : "light"}`}>
      <h2>Where in the world?</h2>
      <button className={`button ${isDarkMode ? "dark" : "light"}`} onClick={toggleTheme}>
        {isDarkMode ? (
          <>
            <img src={iconSun} alt="sun" id="dark"/>
            Light Mode
          </>
        ) : (
          <>
            <img src={iconMoon} alt="monn" id="light"/>
            Dark Mode
          </>
        )}
      </button>
    </div>
  );
};

export default Header;
