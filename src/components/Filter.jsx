import React from "react";
import './Filter.css';
import { useTheme } from "../ThemeContext";

export default function Filter( {filterOption, onFilterChange, countries, onSubregionChange, subregionOption} ) {
    const {theme} = useTheme();
    const isDarkMode = theme === 'dark';

    const regionsData = countries.reduce((regionsArray, country) => {
        if (!regionsArray.includes(country.region) && country.region != 'Antarctic') {
            regionsArray.push(country.region);
        }
        return regionsArray;
    },[]);
    
    const subregionsData = countries.reduce((subregionsArray, country) => {
        if (country.region === filterOption && !(subregionsArray.includes(country.subregion))) {
            subregionsArray.push(country.subregion);
        }
        return subregionsArray;
    },[]);

    return (
        <div className="filter">
            <select id="dropdown-1" value={filterOption} onChange={onFilterChange} className={`select ${isDarkMode ? 'dark' : 'light'}`}>
                <option value=''>Filter by region</option>
                {regionsData.map(region => <option value={region}>{region}</option>)}   
            </select>

            {filterOption && (
                <select id="dropdown-2" value={subregionOption} onChange={onSubregionChange} className={`select ${isDarkMode ? 'dark' : 'light'}`}>
                    <option value=''>Filter by sub-region</option>
                    {subregionsData.map(subregion => <option value={subregion}>{subregion}</option>)}
                </select>
            )}
        </div>
    );
};

