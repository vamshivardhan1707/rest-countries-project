import React from "react";
import './Sort.css';
import { useTheme } from "../ThemeContext";

export default function Sort({ sortOrder, onSortChange, defaultValue }) {
    const {theme} = useTheme();
    const isDarkMode = theme === 'dark';
    
    return (
        <div className="sort">
            <select className={`select ${isDarkMode ? 'dark' : 'light'}`} value={sortOrder} onChange={(event) => onSortChange(event.target.value)}>
                <option value="">{`Sort by ${defaultValue}`}</option>
                <option value="asc">{defaultValue} (ASC)</option>
                <option value='desc'>{defaultValue} (DESC)</option>
            </select>
        </div>
    )
}
