import React from "react";
import { useTheme } from "../ThemeContext";

export default function ErrorMessage({ message }) {
  const { theme } = useTheme();
  const isDarkMode = theme === "dark";
  return (
    <h2 className={`message ${isDarkMode ? "dark" : "light"}`}>{message}</h2>
  );
}
