import React, { useEffect, useState } from "react";
import { useTheme } from "../ThemeContext.jsx";
import { useParams, useNavigate } from "react-router-dom";
import LoadingMessage from "./LoadingMessage";
import ErrorMessage from "./ErrorMessage";
import "./CountryDetail.css";

const CountryDetail = () => {
  const { theme } = useTheme();
  const isDarkMode = theme === "dark";

  const { id } = useParams();
  const navigate = useNavigate();
  const [countryDetail, setCountryDetail] = useState([]);
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    let apiUrl = `https://restcountries.com/v3.1/alpha/${id}`;

    fetch(apiUrl)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Failed to fetch data");
        }
        return response.json();
      })
      .then((data) => {
        setLoading(false);
        console.log(data[0]);
        setCountryDetail(data[0]);
      })
      .catch((err) => {
        console.error(err);
        setErrorMessage("Failed to fetch data");
        setLoading(false);
      });
  }, [id]);

  const handleBorderButtonClick = (border) => {
    navigate(`/country/${border}`);
  };

  return (
    <div className="country-detail">
      {loading && <LoadingMessage />}
      {errorMessage && <ErrorMessage />}
      {!loading && !errorMessage && (
        <div className="country-detail-container">
          <button
            className={`goback ${isDarkMode ? "dark" : "light"}`}
            onClick={() => navigate(-1)}
          >
            Back
          </button>
          <div className="country-detail-content">
            <img
              src={countryDetail.flags.png}
              alt={countryDetail.name.common}
            />
            <div
              className={`country-detail-info ${isDarkMode ? "dark" : "light"}`}
            >
              <h2 className="topic">{countryDetail.name.common}</h2>
              <div className="parts">
                <div className="part-1">
                  <span className="title">Native Name: </span>
                  <span className="details">
                    {Object.values(countryDetail.name.nativeName)[0].common}
                  </span>
                  <br />
                  <span className="title">Population: </span>
                  <span className="details">
                    {countryDetail.population.toLocaleString()}
                  </span>
                  <br />
                  <span className="title">Region: </span>
                  <span className="details">{countryDetail.region}</span>
                  <br />
                  <span className="title">Sub Region: </span>
                  <span className="details">{countryDetail.subregion}</span>
                  <br />
                  <span className="title">Capital: </span>
                  <span className="details">{countryDetail.capital}</span>
                </div>
                <div className="part-2">
                  <span className="title">Top Level Domain: </span>
                  <span className="details">{countryDetail.tld}</span>
                  <br />
                  <span className="title">Currencies: </span>
                  <span className="details">
                    {Object.values(countryDetail.currencies)[0].name}
                  </span>
                  <br />
                  <span className="title">Languages: </span>
                  <span className="details">
                    {Object.values(countryDetail.languages).join(", ")}
                  </span>
                </div>
              </div>
              {countryDetail.borders && (
                <div className="border-buttons">
                  <span className="title">Border Countries:</span>
                  {countryDetail.borders.map((border) => (
                    <button
                      className={`borders ${isDarkMode ? "dark" : "light"}`}
                      onClick={() => handleBorderButtonClick(border)}
                    >
                      {border}
                    </button>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CountryDetail;
