import React, { useState,useEffect } from 'react';
import { useTheme } from '../ThemeContext.jsx';
import './Countries.css';
import Search from './Search.jsx';
import Filter from './Filter.jsx';
import Sort from './Sort.jsx';
import { Link } from 'react-router-dom';
import LoadingMessage from './LoadingMessage.jsx';
import ErrorMessage from './ErrorMessage.jsx';

const Countries = () => {
    const {theme} = useTheme();
    const isDarkMode = theme === 'dark';

    const [countries, setCountries] = useState([]);
    const [searchValue, setSearchValue] = useState('');
    const [filterOption, setFilterOption] = useState('');
    const [subregionOption, setSubregionOption] = useState('');
    const [loading, setLoading] = useState(true);
    const [errorMessage, setErrorMessage] = useState('');
    const [populationSort, setPopulationSort] = useState('');
    const [areaSort, setAreaSort] = useState('');
    
    useEffect(() => {
       const apiUrl = 'https://restcountries.com/v3.1/all';

       fetch(apiUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }
            return response.json();
        })
        .then((data) => {
            console.log(data);
            setCountries(data);
            setLoading(false);
        })
        .catch((err) => {
            console.error(err);
            setErrorMessage('Failed to fetch data');
            setLoading(false);
        })
    }, [])

    const handleSearchChange = (event) => {
        setSearchValue(event.target.value);
    }

    const handleFilterChange = (event) => {
        setFilterOption(event.target.value);
        setSubregionOption('');
    }

    const handleSubregionChange = (event) => {
        setSubregionOption(event.target.value);
    }

    const handleSortByProperty = (value, propertyName) => {
        const sortedCountries = countries.sort((a, b) => {
            return value === 'asc' ? a[propertyName] - b[propertyName] : b[propertyName] - a[propertyName];
        });
        
        if (propertyName === 'population') {
            setPopulationSort(value);
            setAreaSort('');
        } else if (propertyName === 'area') {
            setAreaSort(value);
            setPopulationSort('');
        }
        
        setCountries(sortedCountries);
    };

    const filteredCountries = countries
        .filter((country) => {
            const searchResult = country.name.common.toLowerCase().includes(searchValue.toLowerCase());
            const filterResult = filterOption === '' || country.region === filterOption;
            const subregionFilterResult = subregionOption === '' || country.subregion === subregionOption;
            return searchResult && filterResult && subregionFilterResult;
        });

    return (
        <div className='countries'>
            <div className='features'>
                <Search searchValue={searchValue} onSearchChange={handleSearchChange} />
                <Sort sortOrder={populationSort} onSortChange={(value) => handleSortByProperty(value, 'population')} 
                    defaultValue='Population'/>
                <Sort sortOrder={areaSort} onSortChange={(value) => handleSortByProperty(value, 'area')} 
                    defaultValue='Area' />
                <Filter filterOption={filterOption} onFilterChange={handleFilterChange} 
                    countries={countries} onSubregionChange={handleSubregionChange} 
                    subregionOption={subregionOption} />
            </div>
            <div className="countries-data">
                {loading && <LoadingMessage />}
                {errorMessage && <ErrorMessage message={errorMessage} />}
                {filteredCountries.length > 0 && (
                filteredCountries.map((country) => (
                    <Link key={country.cca3} to={`/country/${country.cca3}`}>
                        <div className={`countries-display ${isDarkMode ? 'dark' : 'light'}`}>
                            <div className='country-flag'>
                                <img src={country.flags.png} />
                            </div>
                            <div className="country-info">
                                <h3>{country.name.common}</h3>
                                <p><span className='title'>Population:</span> <span className='details'>{country.population.toLocaleString()}</span></p>
                                <p><span className='title'>Region:</span> <span className='details'>{country.region}</span></p>
                                <p><span className='title'>Capital:</span> <span className='details'>{country.capital}</span></p>
                                {filterOption && <p><span className='title'>Sub-Region:</span> <span className='details'>{country.subregion}</span></p>}
                                {areaSort && <p><span className='title'>Area:</span> <span className='details'>{country.area}</span></p>}
                            </div>
                        </div>
                    </Link>
                ))
                )}
                {!loading && filteredCountries.length === 0 && <h2 className={`message ${isDarkMode ? 'dark' : 'light'}`}>No such countries found</h2>}
            </div> 
        </div>

    ) 
}

export default Countries