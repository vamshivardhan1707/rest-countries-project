import { Routes, Route } from 'react-router-dom';
import './App.css';
import Countries from './components/Countries';
import CountryDetail from './components/CountryDetail';
import Header from './components/Header';
import { ThemeProvider } from './ThemeContext';



function App() {

  return (
    <ThemeProvider>
      <div className="main-container">
        <header className='header'>
          <Header />
        </header>
        <div className="container">
          <Routes>
            <Route path='/' element={<Countries />} />
            <Route path='country/:id' element={<CountryDetail />} />
          </Routes>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default App;
